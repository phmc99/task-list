import "./style.css";

export function TodoList({ todos, removeTodo }) {
  return (
    <ul className="todo-list">
      {todos.map((item, index) => (
        <li key={index} className="todo-card">
          <div className="task">
            <p>{item}</p>
          </div>
          <button onClick={() => removeTodo(item)}>Concluir</button>
        </li>
      ))}
    </ul>
  );
}
