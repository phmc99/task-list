import { useState } from "react";
import "./style.css";

export function Form({ addTodo }) {
  const [userInput, setUserInput] = useState("");

  return (
    <div className="form-box">
      <textarea
        type="text"
        value={userInput}
        onChange={(event) => setUserInput(event.target.value)}
        placeholder="Digite"
      />
      <button
        onClick={() => {
          setUserInput("");
          return addTodo(userInput);
        }}
      >
        Adicionar
      </button>
    </div>
  );
}
