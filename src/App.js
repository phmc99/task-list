import { useEffect, useState } from "react";

import { TodoList } from "./components/ToDoList";
import { Form } from "./components/Form";

import "./styles/reset.css";
import "./styles/style.css";
import toast, { Toaster } from "react-hot-toast";

export const App = () => {
  const [todo, setTodo] = useState([]);
  const toastStyle = {
    duration: 1500,
    style: {
      background: "#333",
      color: "#fff",
    },
  };

  useEffect(() => {
    const localStorageItens = localStorage.getItem("tasks");
    if (localStorageItens !== null) {
      setTodo(localStorageItens.split(","));
    } else {
      localStorage.setItem("tasks", []);
    }
  }, []);

  const removeTodo = (task) => {
    const removeTask = todo.filter((item) => item !== task);
    localStorage.setItem("tasks", removeTask);
    setTodo(removeTask);
    toast.success("Tarefa concluída! Parabéns!", toastStyle);
  };

  const addTodo = (newTodo) => {
    if (newTodo.trim() !== "") {
      const newTodoList = [...todo, newTodo];
      localStorage.setItem("tasks", newTodoList);
      setTodo(newTodoList);
    } else {
      toast.error("Digite alguma tarefa.", toastStyle);
    }
  };
  return (
    <div className="App">
      <Toaster />
      <Form addTodo={addTodo} />
      <TodoList todos={todo} removeTodo={removeTodo} />
    </div>
  );
};
